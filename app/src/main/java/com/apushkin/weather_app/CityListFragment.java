package com.apushkin.weather_app;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.apushkin.weather_app.content.CitiesListArrayAdapter;
import com.apushkin.weather_app.content.City;
import com.apushkin.weather_app.jsonweather.WeatherData;
import com.apushkin.weather_app.utils.TaskFragment;
import com.apushkin.weather_app.utils.Utils;

import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

//TODO - изменить layout строки, чтобы текст отображался корректно, меньше кегль для длинных

public class CityListFragment extends Fragment implements AdapterView.OnItemClickListener,
        SwipeRefreshLayout.OnRefreshListener {
    protected final static String TAG = "CityListFragment";
    private ListView mListView;

    private CitiesListArrayAdapter mAdapter;
    private TaskFragment mTaskFragment;

    public static final String RETAINED_FRAGMENT_TAG = "RetainedFragment";
    private static final String BUNDLED_CITIES_KEY = "CitiesStoredInBundle";

    private OnListFragmentInteractionListener mListener;
    private WeakReference<MainActivity> mActivity;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private List<City> mCities;


    /**
     * A mandatory empty constructor
     */
    public CityListFragment() {
    }

    @SuppressWarnings("unused")
    public static CityListFragment newInstance() {
        CityListFragment fragment = new CityListFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mActivity = new WeakReference<>((MainActivity)this.getActivity());

        startTask();
        if (savedInstanceState != null){
            //Попытка вытащить сохраненные данные при реконфигурации
            if (mAdapter != null){
                mAdapter.clear();
                mCities = (ArrayList<City>)savedInstanceState
                        .getSerializable(BUNDLED_CITIES_KEY);
                mAdapter.addAll(mCities);
            }
            mAdapter.notifyDataSetChanged();
        }  else {
            if(mAdapter == null){
                mAdapter = new CitiesListArrayAdapter(getContext());
                mAdapter.addAll(getCitiesList());
            }
        }

        mListView.setAdapter(mAdapter);
        mListView.setOnItemClickListener(this);
    }

    private void startTask() {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        mTaskFragment = (TaskFragment) fm.findFragmentByTag(RETAINED_FRAGMENT_TAG);

        if(mTaskFragment == null) {
            mTaskFragment = TaskFragment.newInstance(mActivity.get().getCitiesList());
            fm
                    .beginTransaction()
                    .add(mTaskFragment, RETAINED_FRAGMENT_TAG)
                    .commit();
        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        List<City> cityList = mAdapter.getCitiesFromAdapter();
        outState.putSerializable(BUNDLED_CITIES_KEY, (Serializable) cityList);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_city_list_listview, container, false);
        mListView = (ListView) view.findViewById(R.id.cityListListView);

        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.refresh);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.swipe_light_blue,
                R.color.swipe_blue, R.color.colorPrimaryDark);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        return view;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if(mListener != null) {
            mListener.onListFragmentInteraction(mAdapter.getItem(position));
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(City item);
    }

    @Override
    public void onPause() {
        super.onPause();
        mActivity.get().setCurrentFragment(MainActivity.FragmentType.CITY_LIST);
        mActivity.clear();
    }

    @Override
    public void onResume() {
        super.onResume();
        mActivity = new WeakReference<>((MainActivity)this.getActivity());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mActivity.clear();
    }

    @Override
    public void onRefresh() {
        mSwipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                for (City city: mAdapter.getCitiesFromAdapter()){
                    if(mTaskFragment != null)
                        mTaskFragment.runWeatherTaskForNewCity(city.getDisplayName());
                    else Log.d(TAG, "TaskFragment was not found");
                }
            }
        });
    }

    /**
     * Performs start of refresing is @isStarting is true
     * and stop of refreshing otherwise
     * @param isStarting
     */
    public void startOrStopRefreshing(boolean isStarting) {
        if (isStarting) {
            mSwipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.setRefreshing(true);
                }
            });
        } else {
            mSwipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.setRefreshing(false);
                }
            });
        }
    }

    private List<City> getCitiesList() {
       List<String> names = mActivity.get().getCitiesList();
       List<City> citiesList = new ArrayList<>();
       for (String name : names) {
           City city = new City();
           city.setDisplayName(name);
           citiesList.add(city);
       }
        return  citiesList;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    public void onPostExecute(List<WeatherData> results, String errorMessage) {
        startOrStopRefreshing(false);

        if(results == null){
            Utils.showToast(getContext(), errorMessage);
            return;
        }

        double temperature = results.get(0).mTemp;
        String displayName = results.get(0).mDisplayName;
        Log.d(TAG, "Returned result for " + displayName + ", t = " + temperature);

        if (mAdapter != null) {
            City city = mAdapter.getItem(displayName);
            city.setWeather(results.get(0));
            mAdapter.notifyDataSetChanged();
        }
    }

    public void addCity(String cityDisplayName) {
        City city = new City();
        city.setDisplayName(cityDisplayName);

        if (null != mAdapter) {
            mAdapter.add(city);
            if (mTaskFragment == null){
                mTaskFragment = (TaskFragment) getFragmentManager()
                        .findFragmentByTag(RETAINED_FRAGMENT_TAG);
            }

            if (mTaskFragment != null){
                    mTaskFragment.runWeatherTaskForNewCity(cityDisplayName);
            }
            mAdapter.notifyDataSetChanged();
        }
    }

    private String getStringFromCache(String key){
        refreshmActivity();
        return mActivity.get().getFromPreferences(key);
    }

    private void refreshmActivity(){
        if (mActivity == null){
            mActivity = new WeakReference<>((MainActivity) this.getActivity());
        }
    }
}

