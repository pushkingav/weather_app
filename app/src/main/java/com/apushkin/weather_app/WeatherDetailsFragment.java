package com.apushkin.weather_app;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.apushkin.weather_app.content.City;
import com.apushkin.weather_app.content.ForecastArrayAdapter;
import com.apushkin.weather_app.jsonweather.forecast.WeatherForecast;
import com.apushkin.weather_app.jsonweather.forecast.WeatherForecastListEntry;
import com.apushkin.weather_app.utils.TaskFragment;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class WeatherDetailsFragment extends Fragment
        implements SwipeRefreshLayout.OnRefreshListener {
    public static final String TAG = "WeatherDetailsFragment";

    private OnListFragmentInteractionListener mListener;
    private WeakReference<MainActivity> mActivity;

    private ForecastArrayAdapter mAdapter;
    private ListView mListView;
    private TextView mCityNameTv;
    private TextView mTemperatureTv;
    private TextView mPressureTv;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    private City mCity;
    private WeatherForecast mForecast;
    private TaskFragment mTaskFragment;

    public WeatherDetailsFragment() {
    }

    @SuppressWarnings("unused")
    public static WeatherDetailsFragment newInstance(City city) {
        WeatherDetailsFragment fragment = new WeatherDetailsFragment();
        Bundle args = new Bundle();
        args.putSerializable("City", city);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.weather_details, container, false);
        mActivity = new WeakReference<>((MainActivity)(getActivity()));
        mAdapter = new ForecastArrayAdapter(getContext());

        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.refresh_forecast);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.swipe_light_blue,
                                                    R.color.swipe_blue,
                                                    R.color.colorPrimaryDark);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        if (getArguments() != null) {
            this.mCity = (City)getArguments().getSerializable("City");
        }

        mTaskFragment = (TaskFragment) mActivity.get()
                .getSupportFragmentManager()
                .findFragmentByTag(CityListFragment.RETAINED_FRAGMENT_TAG);

        if (mTaskFragment!= null) {
            startOrStopRefreshing(true);
            mTaskFragment.runForecastTask(mCity.getDisplayName(), false);
        }

        mListView = (ListView) view.findViewById(R.id.list_forecast);
        mListView.setAdapter(mAdapter);

        if(mCity != null){
            mCityNameTv = (TextView) view.findViewById(R.id.city_name_details_textView);
            mCityNameTv.setText(mCity.getDisplayName());

            mTemperatureTv = (TextView) view.findViewById(R.id.temperature_text_view);
            String temp;
            Double convertedPressure;
            String pressure;

            if (mCity.getWeather() != null){
                temp = String.valueOf(Math.round(mCity.getWeather().mTemp)) + " \u2103";
                //Переведем hpa в мм рт.ст. и округлим до целого
                convertedPressure = (double) Math
                        .round(mCity.getWeather().mPressure * 0.750062D);
                pressure = String.valueOf(convertedPressure) +
                        " " + getString(R.string.pressure_suffix);
            } else {
                temp = "N/A";
                pressure = "N/A";            }
            mTemperatureTv.setText(temp);

            mPressureTv = (TextView) view.findViewById(R.id.pressure_text_view);
            mPressureTv.setText(pressure);
        }
        return view;
    }

    @Override
    public void onResume() {
        if (mActivity == null) {
            mActivity = new WeakReference<>((MainActivity) getActivity());
        }
        super.onResume();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onPause() {
        super.onPause();
        mActivity.get().setCurrentFragment(MainActivity.FragmentType.WEATHER_DETAILS);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        mActivity = null;
    }

    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(City item);
    }

    public static String getTAG() {
        return TAG;
    }

    public void onPostExecute(WeatherForecast forecast, String errorMessage) {
       startOrStopRefreshing(false);

        mForecast = forecast;
        if (mForecast != null) {
            List<CityItem> adapterItems = new ArrayList<>();

            List<WeatherForecastListEntry> entries= mForecast.getList();
            for (WeatherForecastListEntry listEntry : entries) {
                CityItem item = new CityItem(listEntry.getDtTxt(),
                                                    listEntry.getMain().getTemp());
                adapterItems.add(item);
            }
            if(mAdapter != null){
                mAdapter.addAll(adapterItems);
                mAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onRefresh() {
        mSwipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                    if(mTaskFragment != null)
                        mTaskFragment.runForecastTask(mCity.getDisplayName(), true);
                    else Log.d(TAG, "TaskFragment was not found");
            }
        });
    }

    /**
     * Performs start of refresing is @isStarting is true
     * and stop of refreshing otherwise
     * @param isStarting
     */
    public void startOrStopRefreshing(boolean isStarting) {
        if (isStarting) {
            mSwipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.setRefreshing(true);
                }
            });
        } else {
            mSwipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.setRefreshing(false);
                }
            });
        }
    }

    public static class CityItem {
        private String date;
        private double temp;

        public CityItem(String date, double temp) {
            this.date = date;
            this.temp = temp;
        }

        public String getDate() {
            return date;
        }

        public double getTemp() {
            return temp;
        }
    }
}
