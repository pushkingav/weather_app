package com.apushkin.weather_app;
//TODO - fix fab button that appears after reconfiguring the WeatherDetailsFragment (посмотреть какой был фрагмент и в зависисмости от этого принять решение)
//TODO - пофиксить некорректную сортировку списка городов после рестарта приложения


/*Необходимо создать погодное приложение.
  Приложение должно иметь минимум 2 встроенных города (Тула, Москва), пользователь может добавить
  свой город.  Приложение должно выводить города с указание температуры, приложение должно
  уметь показывать более подробную информацию по городу и уметь показывать прогноз погоды
  (3 или 7 дней). http://developer.yahoo.com/weather, или на http://openweathermap.org/API,
  или любой другой на выбор. Сторонние библиотеки, за исключением, если необходимого функционала
  нет в стандартном API, использовать запрещается. Необходимо продемонстрировать знание ООП,
  хранения данных, многопоточного программирования,
  обеспечение отказоустойчивости и умение проектировки интерфейсов.*/

import android.app.Activity;
import android.app.Dialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;

import com.apushkin.weather_app.content.City;
import com.apushkin.weather_app.jsonweather.WeatherData;
import com.apushkin.weather_app.jsonweather.forecast.WeatherForecast;
import com.apushkin.weather_app.utils.TaskFragment;
import com.apushkin.weather_app.utils.Utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        CityListFragment.OnListFragmentInteractionListener, TaskFragment.WeatherTaskCallbacks,
        TaskFragment.ForecastTaskCallbacks {

    public static final String TAG = "MainActivity";
    private static final String CITIES_KEY = "CitiesStoredInSharedPrefs";
    private static final String BUNDLE_CITIES_KEY = "CitiesStoredInBundle";
    private static final String BUNDLE_CURR_FRAGMENT_KEY ="CurrentFragmentStoredInBundle";

    private FragmentManager mFragmentManager;
    private CityListFragment cityListFragment;
    private WeatherDetailsFragment weatherDetailsFragment;
    private ActionBarDrawerToggle toggle;
    private DialogFragment dialogFragment;
    private SharedPreferences mPreferences;

    private FloatingActionButton fab;

    protected enum FragmentType {
        CITY_LIST, WEATHER_DETAILS, NOT_SET
    }

    public enum JSONType {
        WEATHER, FORECAST
    }

    //Список городов
    private ArrayList<String> mCitiesList;

    //Used to track the current shown Fragment
    private FragmentType currentFragment = FragmentType.NOT_SET;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        if (savedInstanceState != null) {
            currentFragment = (FragmentType) savedInstanceState
                    .getSerializable(BUNDLE_CURR_FRAGMENT_KEY);
            mCitiesList = savedInstanceState.getStringArrayList(BUNDLE_CITIES_KEY);
        }

        //Если глобальный лист с именами пустой
        if(mCitiesList == null || mCitiesList.isEmpty()){
            //почитаем набор из преференций
            HashSet<String> savedCities = (HashSet<String>) mPreferences
                    .getStringSet(CITIES_KEY, null);
            if (savedCities != null) {
                //Если в преференциях не пусто, то передаем набор из них в глобальный лист
                mCitiesList = new ArrayList<>(savedCities);
            } else {
                //Иначе инициировать первичный список с двумя "предустановленными" городами
                initializeCitiesList();
            }
        }

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fab = (FloatingActionButton) findViewById(R.id.fab);
        if (fab != null) {
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialogFragment = AlertDialogFragment.newInstance();
                    dialogFragment.show(mFragmentManager, AlertDialogFragment.TAG );
                }
            });
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        if (drawer != null) {
            drawer.addDrawerListener(toggle);
        }
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        if (navigationView != null) {
            navigationView.setNavigationItemSelectedListener(this);
        }

        mFragmentManager = getSupportFragmentManager();
        mFragmentManager.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                int backStackEntryCount = getSupportFragmentManager().getBackStackEntryCount();
                if (backStackEntryCount > 0) {
                    resetIndicator();
                    toggle.setHomeAsUpIndicator(null);
                    toggle.setDrawerIndicatorEnabled(false);
                    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                    fab.hide();
                } else {
                    resetIndicator();
                    fab.show();
                }
            }
        });

        if (mFragmentManager.getBackStackEntryCount() > 0) {
            toggle.setHomeAsUpIndicator(null);
            toggle.setDrawerIndicatorEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        //Find existing or create new fragment
        if (currentFragment.equals(FragmentType.NOT_SET)) {
            cityListFragment = CityListFragment.newInstance();
            showTheExistingFragment(cityListFragment, CityListFragment.TAG);
        } else {
            switch (currentFragment) {
                case CITY_LIST:
                    cityListFragment = (CityListFragment) mFragmentManager
                            .findFragmentByTag(CityListFragment.TAG);
                    if (cityListFragment == null) {
                        cityListFragment = CityListFragment.newInstance();
                    }
                    showTheExistingFragment(cityListFragment, CityListFragment.TAG);
                    break;

                case WEATHER_DETAILS:
                    weatherDetailsFragment = (WeatherDetailsFragment) mFragmentManager
                            .findFragmentByTag(WeatherDetailsFragment.TAG);
                    showTheExistingFragment(weatherDetailsFragment, WeatherDetailsFragment.TAG);
                    break;
            }
        }
    }

    private void resetIndicator(){
        //Следующая команда управляет видимостью иконки меню в виде пирога
        toggle.setDrawerIndicatorEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        toggle.syncState();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(BUNDLE_CURR_FRAGMENT_KEY, currentFragment);
        outState.putStringArrayList(BUNDLE_CITIES_KEY, mCitiesList);
    }

    private void showTheExistingFragment(Fragment fragment, String tag) {
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.content_main, fragment,
                tag);
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.commit();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer != null) {
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                if (getSupportFragmentManager().getBackStackEntryCount() > 0){
                    getSupportFragmentManager().popBackStack();
                    toggle.syncState();
                } else {
                    super.onBackPressed();
                }
            }
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_reset) {
            invalidateCache();

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer != null) {
            drawer.closeDrawer(GravityCompat.START);
        }
        return true;
    }

    @Override
    public void onListFragmentInteraction(City item) {

        weatherDetailsFragment = (WeatherDetailsFragment) mFragmentManager
                .findFragmentByTag(WeatherDetailsFragment.TAG);
        if (weatherDetailsFragment == null) {
            Log.d(TAG, "WeatherDetailsFragment is not found - creating a new Instance of it");
            weatherDetailsFragment = WeatherDetailsFragment.newInstance(item);
        }

        //Show the fragment
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.content_main, weatherDetailsFragment,
                WeatherDetailsFragment.TAG);
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.addToBackStack(weatherDetailsFragment.getTag());
        fragmentTransaction.commit();
    }


    public FragmentType getCurrentFragment() {
        return currentFragment;
    }

    public void setCurrentFragment(FragmentType currentFragment) {
        this.currentFragment = currentFragment;
    }
//--------------------------- callbacks---------------------------------------
    @Override
    public void onCancelled() {

    }

    @Override
    public void onPostExecute(List<WeatherData> results, String errorMessage) {
        String raw = "";
        if(results != null){
            raw = results.get(0).mRawJSON;
        }
        if(!raw.isEmpty()){
            saveToPreferences(results.get(0).mDisplayName + "weather", raw);
        }
        if (cityListFragment != null) {
            cityListFragment.onPostExecute(results, errorMessage);
        }
    }

    @Override
    public void onPostExecute(WeatherForecast result, String errorMessage) {
        String raw = "";
        if(result != null){
            raw = result.getRaw_Json();
        }
        if(!raw.isEmpty()){
            saveToPreferences(result.getDisplayName() + "forecast", raw);
        }

        if(weatherDetailsFragment != null) {
            weatherDetailsFragment.onPostExecute(result, errorMessage);
        }
    }

    //----------------------------------------------------------------------------

    public void continueDialog(String cityName){
        if(dialogFragment == null) {
            dialogFragment = (DialogFragment) mFragmentManager
                    .findFragmentByTag(AlertDialogFragment.TAG);
        }
        dialogFragment.dismiss();
        if (null != cityName && !cityName.isEmpty()){
            if(null != cityListFragment) {
                cityListFragment.addCity(cityName);
                addCityAndSavePreferences(cityName);
            }
        } else {
            Utils.showToast(this, "По данному запросу ничего не нашлось");
        }
    }

    // Class for creating a new DialogFragment
    public static class AlertDialogFragment extends DialogFragment {

        public static final String TAG = AlertDialogFragment.class.getCanonicalName();

        public static AlertDialogFragment newInstance() {
            return new AlertDialogFragment();
        }



        @Override
        public void onAttach(Activity activity) {

            super.onAttach(activity);
        }

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final String[] CITIES = getResources().getStringArray(R.array.citiesListTxt);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),
                    R.layout.list_autocomplete_item, CITIES);

            // Get the layout inflater
            LayoutInflater inflater = getActivity().getLayoutInflater();
            View dialogLayout = inflater.inflate(R.layout.add_city_dialogue, null);
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setView(dialogLayout);
            Button buttonOk = (Button) dialogLayout.findViewById(R.id.ok_button);

            final AutoCompleteTextView tv = (AutoCompleteTextView) dialogLayout
                    .findViewById(R.id.autoCompleteTextView);
            tv.setAdapter(adapter);

            final List<String> citiesArrayList = new ArrayList<>(Arrays.asList(CITIES));

            buttonOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String entered = tv.getText().toString();
                    if (citiesArrayList.contains(entered)){
                        ((MainActivity)getActivity()).continueDialog(tv.getText().toString());
                    } else {
                        ((MainActivity)getActivity()).continueDialog(null);
                    }
                }
            });



            return builder.create();
        }
    }

    //For fragments
    public ArrayList<String> getCitiesList() {
        return mCitiesList;
    }

    public void setCitiesList(ArrayList<String> mCitiesList) {
        this.mCitiesList = mCitiesList;
    }

    /** Adds a new city to global list and saves preferences
     *
     * @param newName   New City's name to add to list
     */
    private void addCityAndSavePreferences(String newName){

        Set<String> namesSet = mPreferences.getStringSet(CITIES_KEY, null);
        if (namesSet != null) {
            namesSet.add(newName);
            mCitiesList = new ArrayList<>(namesSet);
            saveCitiesListToPreferences();
        }
    }

    private void initializeCitiesList() {
        Set<String> namesSet = new HashSet<>();
        namesSet.add(getString(R.string.city_tula));
        namesSet.add(getString(R.string.city_moscow));
        mCitiesList = new ArrayList<>(namesSet);
        saveCitiesListToPreferences();
    }

    private void saveCitiesListToPreferences() {
        refreshPreferences();
        SharedPreferences.Editor editor = mPreferences.edit();
        HashSet<String> citiesSet = new HashSet<>(mCitiesList);
        editor.putStringSet(CITIES_KEY, citiesSet).apply();
    }

    public ArrayList<String> getCitiesFromPreferences() {
        refreshPreferences();
        HashSet<String> names = (HashSet<String>) mPreferences.getStringSet(CITIES_KEY, null);
        return new ArrayList<>(names);
    }

    public void saveToPreferences(String key, String value){
        refreshPreferences();
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putString(key, value).apply();
    }

    public String getFromPreferences(String key) {
        refreshPreferences();
        return mPreferences.getString(key, null);
    }

    private void refreshPreferences() {
        if (mPreferences == null) {
            mPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        }
    }

    private void invalidateCache() {
        //Clear preferences
        refreshPreferences();
        mPreferences.edit().clear().apply();

        //Load default cities and save them to preferences
        initializeCitiesList();
        setCitiesList(getCitiesFromPreferences());

        //Trying to renew CityListFragment - it should not be possible that it is null
        if(cityListFragment == null) {
            cityListFragment = (CityListFragment) getSupportFragmentManager()
                    .findFragmentByTag(CityListFragment.TAG);
        }

        //Prepare for delete and replace Fragments with the new ones
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

        //Find the TaskFragment
        TaskFragment taskFragment = (TaskFragment) getSupportFragmentManager()
                .findFragmentByTag(CityListFragment.RETAINED_FRAGMENT_TAG);

        //Remove Task and CityList Fragments
        if(taskFragment != null){
            ft.remove(taskFragment);
        }
        ft.remove(cityListFragment);
        ft.commit();
        getSupportFragmentManager().executePendingTransactions();

        /**
         * Add new CityListFragment and start new AsyncTasks in a new TaskFragment for reloaded
         * cities */
        cityListFragment = CityListFragment.newInstance();
        showTheExistingFragment(cityListFragment, CityListFragment.TAG);
    }

    public String getSavedItemForCity(String cityName, JSONType type) {
        refreshPreferences();
        if (type == JSONType.WEATHER) {
            cityName += "weather";
        } else {
            cityName += "forecast";
        }
        return mPreferences.getString(cityName, null);
    }

}
