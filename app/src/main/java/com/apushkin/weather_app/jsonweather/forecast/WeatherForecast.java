package com.apushkin.weather_app.jsonweather.forecast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WeatherForecast {
    static final String cod_JSON = "cod";
    static final String message_JSON = "message";
    static final String cnt_JSON = "cnt";
    static final String city_JSON = "city";
    static final String list_JSON = "list";

    private Long cod;
    private Double message;
    private Long cnt;
    private City city;
    private List<WeatherForecastListEntry> weatherForecastList
            = new ArrayList<>();
    private Map<String, Object> additionalProperties = new HashMap<>();
    private String raw_Json;
    private String displayName;

    /**
     * No args constructor for use in serialization
     *
     */
    public WeatherForecast() {
    }

    /**
     *
     * @param message
     * @param cnt
     * @param cod
     * @param weatherForecastList
     * @param city
     */
    public WeatherForecast(City city, Long cod, Double message, Long cnt, List<WeatherForecastListEntry> weatherForecastList) {
        this.city = city;
        this.cod = cod;
        this.message = message;
        this.cnt = cnt;
        this.weatherForecastList = weatherForecastList;
    }

    public String getRaw_Json() {
        return raw_Json;
    }

    public void setRaw_Json(String raw_Json) {
        this.raw_Json = raw_Json;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    /**
     *
     * @return
     * The city
     */
    public City getCity() {
        return city;
    }

    /**
     *
     * @param city
     * The city
     */
    public void setCity(City city) {
        this.city = city;
    }

    /**
     *
     * @return
     * The cod
     */
    public Long getCod() {
        return cod;
    }

    /**
     *
     * @param cod
     * The cod
     */
    public void setCod(Long cod) {
        this.cod = cod;
    }

    /**
     *
     * @return
     * The message
     */
    public Double getMessage() {
        return message;
    }

    /**
     *
     * @param message
     * The message
     */
    public void setMessage(Double message) {
        this.message = message;
    }

    /**
     *
     * @return
     * The cnt
     */
    public Long getCnt() {
        return cnt;
    }

    /**
     *
     * @param cnt
     * The cnt
     */
    public void setCnt(Long cnt) {
        this.cnt = cnt;
    }

    /**
     *
     * @return
     * The weatherForecastList
     */
    public List<WeatherForecastListEntry> getList() {
        return weatherForecastList;
    }

    /**
     *
     * @param weatherForecastListEntry
     * The weatherForecastList
     */
    public void setList(List<WeatherForecastListEntry> weatherForecastListEntry) {
        this.weatherForecastList = weatherForecastListEntry;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}