package com.apushkin.weather_app.jsonweather.forecast;
import java.util.ArrayList;
import java.util.List;

public class WeatherForecastListEntry {

    public static final String dt_JSON = "dt";
    public static final String main_JSON = "main";
    public static final String weathers_JSON = "weather";
    public static final String clouds_JSON = "clouds";
    public static final String wind_JSON = "wind";
    public static final String rain_JSON = "rain";
    public static final String snow_JSON = "snow";
    public static final String sys_JSON = "sys";
    public static final String dtTxt_JSON = "dt_txt";

    private Long dt;
    private Main main;
    private List<Weather> weathers = new ArrayList<>();
    private Clouds clouds;
    private Wind wind;
    private Rain rain;
    private Snow snow;
    private Sys_ sys;
    private String dtTxt;

    public WeatherForecastListEntry() {
    }

    public WeatherForecastListEntry(Long dt, Main main, List<Weather> weathers, Clouds clouds, Wind wind, Rain rain, Snow snow, Sys_ sys, String dtTxt) {
        this.dt = dt;
        this.main = main;
        this.weathers = weathers;
        this.clouds = clouds;
        this.wind = wind;
        this.rain = rain;
        this.snow = snow;
        this.sys = sys;
        this.dtTxt = dtTxt;
    }

    /**
     *
     * @return
     * The dt
     */
    public Long getDt() {
        return dt;
    }

    /**
     *
     * @param dt
     * The dt
     */
    public void setDt(Long dt) {
        this.dt = dt;
    }

    /**
     *
     * @return
     * The main
     */
    public Main getMain() {
        return main;
    }

    /**
     *
     * @param main
     * The main
     */
    public void setMain(Main main) {
        this.main = main;
    }

    /**
     *
     * @return
     * The weathers
     */
    public List<Weather> getWeathers() {
        return weathers;
    }

    /**
     *
     * @param weathers
     * The weathers
     */
    public void setWeathers(List<Weather> weathers) {
        this.weathers = weathers;
    }

    /**
     *
     * @return
     * The clouds
     */
    public Clouds getClouds() {
        return clouds;
    }

    /**
     *
     * @param clouds
     * The clouds
     */
    public void setClouds(Clouds clouds) {
        this.clouds = clouds;
    }

    /**
     *
     * @return
     * The wind
     */
    public Wind getWind() {
        return wind;
    }

    /**
     *
     * @param wind
     * The wind
     */
    public void setWind(Wind wind) {
        this.wind = wind;
    }

    /**
     *
     * @return
     * The rain
     */
    public Rain getRain() {
        return rain;
    }

    /**
     *
     * @param rain
     * The rain
     */
    public void setRain(Rain rain) {
        this.rain = rain;
    }

    /**
     *
     * @return
     * The snow
     */
    public Snow getSnow() {
        return snow;
    }

    /**
     *
     * @param snow
     * The snow
     */
    public void setSnow(Snow snow) {
        this.snow = snow;
    }

    /**
     *
     * @return
     * The sys
     */
    public Sys_ getSys() {
        return sys;
    }

    /**
     *
     * @param sys
     * The sys
     */
    public void setSys(Sys_ sys) {
        this.sys = sys;
    }

    /**
     *
     * @return
     * The dtTxt
     */
    public String getDtTxt() {
        return dtTxt;
    }

    /**
     *
     * @param dtTxt
     * The dt_txt
     */
    public void setDtTxt(String dtTxt) {
        this.dtTxt = dtTxt;
    }

}