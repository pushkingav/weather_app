package com.apushkin.weather_app.jsonweather.forecast;

import java.util.HashMap;
import java.util.Map;

public class Sys {

    private Long population;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public Sys() {
    }

    /**
     *
     * @param population
     */
    public Sys(Long population) {
        this.population = population;
    }

    /**
     *
     * @return
     * The population
     */
    public Long getPopulation() {
        return population;
    }

    /**
     *
     * @param population
     * The population
     */
    public void setPopulation(Long population) {
        this.population = population;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}