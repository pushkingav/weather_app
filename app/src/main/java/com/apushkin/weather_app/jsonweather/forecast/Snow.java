package com.apushkin.weather_app.jsonweather.forecast;

public class Snow {

    public static final String _3h_JSON = "3h";
    private Double _3h;

    /**
     * No args constructor for use in serialization
     *
     */
    public Snow() {
    }

    /**
     *
     * @param _3h
     */
    public Snow(Double _3h) {
        this._3h = _3h;
    }

    /**
     *
     * @return
     * The _3h
     */
    public Double get3h() {
        return _3h;
    }

    /**
     *
     * @param _3h
     * The 3h
     */
    public void set3h(Double _3h) {
        this._3h = _3h;
    }

}