package com.apushkin.weather_app.jsonweather.forecast;

public class Sys_ {

    public static final String pod_JSON = "pod";
    private String pod;

    /**
     * No args constructor for use in serialization
     *
     */
    public Sys_() {
    }

    /**
     *
     * @param pod
     */
    public Sys_(String pod) {
        this.pod = pod;
    }

    /**
     *
     * @return
     * The pod
     */
    public String getPod() {
        return pod;
    }

    /**
     *
     * @param pod
     * The pod
     */
    public void setPod(String pod) {
        this.pod = pod;
    }

}
