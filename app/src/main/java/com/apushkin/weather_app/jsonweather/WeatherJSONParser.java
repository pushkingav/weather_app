package com.apushkin.weather_app.jsonweather;

import android.util.JsonReader;
import android.util.JsonToken;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Parses the Json weather data returned from the Weather Services API
 * and returns a WeatherForecastListEntry of JsonWeather objects that contain this data.
 */
public class WeatherJSONParser {
    /**
     * Used for logging purposes.
     */
    private final String TAG =
        this.getClass().getCanonicalName();

    /**
     * Parse the @a inputStream and convert it into a WeatherForecastListEntry of JsonWeather
     * objects.
     */
    public List<JsonWeather> parseJsonStream(InputStream inputStream)
        throws IOException {
        try (JsonReader reader =
                     new JsonReader(new InputStreamReader(inputStream,
                             "UTF-8"))) {
             Log.d(TAG, "Parsing the results returned as an array");
             Log.d(TAG, "Reader consists of: " + reader.toString());

            // Handle the array returned from the Weather Service.
            return parseJsonWeatherArray(reader);
        }
    }

    /**
     * Parse a Json stream and convert it into a WeatherForecastListEntry of JsonWeather
     * objects.
     */
    public List<JsonWeather> parseJsonWeatherArray(JsonReader reader)
        throws IOException {

        List<JsonWeather> weathers = new ArrayList<JsonWeather>();
//        reader.beginObject();
        try {
            // If the data wasn't expanded return null;
            if (reader.peek() == JsonToken.END_OBJECT){
                Log.d(TAG, "!!!Reader returned null!!!!!!!!!!!!!!!!!!!!!!");
                return null;
            }

            if(reader.peek() == JsonToken.BEGIN_OBJECT){
                //while (reader.hasNext())
                weathers.add(parseJsonWeather(reader));
            }
            if (weathers.get(0) == null){
                Log.d(TAG, "Returning null from weather parsing");
                return null;
            } else{
                Log.d(TAG, "Returning weathers: " + weathers.get(0).toString());

                return weathers;
            }


        } finally {
            reader.close();
        }
    }

    /**
     * Parse a Json stream and return a JsonWeather object.
     */
    public JsonWeather parseJsonWeather(JsonReader reader) throws IOException {

        JsonWeather jsonWeatherObject = new JsonWeather();
        reader.beginObject();

        try {
            while (reader.hasNext()) {
                String name = reader.nextName();
                switch (name) {
                    case JsonWeather.cod_JSON:
                        jsonWeatherObject.setCod(reader.nextLong());
                        Log.d(TAG, "read cod field: " + jsonWeatherObject.getCod());
                        break;
                    case JsonWeather.name_JSON:
                        jsonWeatherObject.setName(reader.nextString());
                        break;
                    case JsonWeather.id_JSON:
                        jsonWeatherObject.setId(reader.nextLong());
                        break;
                    case JsonWeather.dt_JSON:
                        jsonWeatherObject.setDt(reader.nextLong());
                        break;
                    case JsonWeather.wind_JSON:
                        jsonWeatherObject.setWind(parseWind(reader));
                        break;
                    case JsonWeather.main_JSON:
                        jsonWeatherObject.setMain(parseMain(reader));
                        break;
                    case JsonWeather.weather_JSON:
                        jsonWeatherObject.setWeather(parseWeathers(reader));
                        break;
                    case JsonWeather.sys_JSON:
                        jsonWeatherObject.setSys(parseSys(reader));
                        break;
                    default:
                        reader.skipValue();
                        // Log.d(TAG, "weird problem with " + name + " field");
                        break;
                }
            }
        } finally {
            reader.endObject();
        }
        return jsonWeatherObject.getCod() == 404L?
                null : jsonWeatherObject;
    }
    
    /**
     * Parse a Json stream and return a WeatherForecastListEntry of Weather objects.
     */
    public List<Weather> parseWeathers(JsonReader reader) throws IOException {
        reader.beginArray();

        try {
            List<Weather> weathers = new ArrayList<Weather>();

            while (reader.hasNext())
                weathers.add(parseWeather(reader));

            return weathers;
        } finally {
            reader.endArray();
        }
    }

    /**
     * Parse a Json stream and return a Weather object.
     */
    public Weather parseWeather(JsonReader reader) throws IOException {
        reader.beginObject();

        Weather weatherObject = new Weather();
        try {
            while (reader.hasNext()){
                String name = reader.nextName();
                switch (name){
                    case Weather.id_JSON:
                        weatherObject.setId(reader.nextLong());
                        break;
                    case Weather.main_JSON:
                        weatherObject.setMain(reader.nextString());
                        break;
                    case Weather.description_JSON:
                        weatherObject.setDescription(reader.nextString());
                        break;
                    case Weather.icon_JSON:
                        weatherObject.setIcon(reader.nextString());
                        break;
                    default:
                        reader.skipValue();
                }
            }
        } finally {

            reader.endObject();
        }

        return weatherObject;
    }
    
    /**
     * Parse a Json stream and return a Main Object.
     */
    public Main parseMain(JsonReader reader) 
        throws IOException {
        //"main":{"temp":306.15,"pressure":1013,"humidity":44,"temp_min":306,"temp_max":306}
        reader.beginObject();

        Main mainObject = new Main();
        try {
            while (reader.hasNext()){
                String name = reader.nextName();
                switch (name){
                    case Main.temp_JSON:
                        mainObject.setTemp(reader.nextDouble());
                        break;
                    case Main.humidity_JSON:
                        mainObject.setHumidity(reader.nextLong());
                        break;
                    case Main.tempMin_JSON:
                        mainObject.setTempMin(reader.nextDouble());
                        break;
                    case Main.tempMax_JSON:
                        mainObject.setTempMax(reader.nextDouble());
                        break;
                    case Main.pressure_JSON:
                        mainObject.setPressure(reader.nextDouble());
                        break;
                    case Main.seaLevel_JSON:
                        mainObject.setSeaLevel(reader.nextDouble());
                        break;
                    case Main.grndLevel_JSON:
                        mainObject.setGrndLevel(reader.nextDouble());
                        break;
                    default:
                        reader.skipValue();
                        break;
                }
            }
        } finally {
            reader.endObject();
        }
        return mainObject;
    }

    /**
     * Parse a Json stream and return a Wind Object.
     */
    public Wind parseWind(JsonReader reader) throws IOException {
        reader.beginObject();

        Wind windObject = new Wind();
        try {
            while (reader.hasNext()){
                String name = reader.nextName();
                switch (name){
                    case Wind.speed_JSON:
                        windObject.setSpeed(reader.nextDouble());
                        break;
                    case Wind.deg_JSON:
                        windObject.setDeg(reader.nextDouble());
                        break;
                    default:
                        reader.skipValue();
                        break;
                }
            }
        } finally {
            reader.endObject();
        }
        return windObject;
    }

    /**
     * Parse a Json stream and return a Sys Object.
     */
    public Sys parseSys(JsonReader reader) throws IOException {
        reader.beginObject();

        Sys sysObject = new Sys();
        try{
            while (reader.hasNext()){
                String name = reader.nextName();
                switch (name){
                    case Sys.message_JSON:
                        sysObject.setMessage(reader.nextDouble());
                        break;
                    case Sys.country_JSON:
                        sysObject.setCountry(reader.nextString());
                        break;
                    case Sys.sunrise_JSON:
                        sysObject.setSunrise(reader.nextLong());
                        break;
                    case Sys.sunset_JSON:
                        sysObject.setSunset(reader.nextLong());
                        break;
                    default:
                        reader.skipValue();
                        break;
                }
            }
        } finally {

            reader.endObject();
        }
        return sysObject;
    }
}
