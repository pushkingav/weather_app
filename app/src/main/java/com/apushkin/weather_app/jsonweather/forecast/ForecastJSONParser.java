package com.apushkin.weather_app.jsonweather.forecast;
// http://api.openweathermap.org/data/2.5/forecast?q=%D0%A2%D1%83%D0%BB%D0%B0&units=metric&APPID=7fdbd1f3d3fdd49668c1243183698d6f

import android.util.JsonReader;
import android.util.JsonToken;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Parses the Json weather data returned from the Weather Services API
 * and returns a WeatherForecastListEntry of JsonWeather objects that contain this data.
 */
public class ForecastJSONParser {
    /**
     * Used for logging purposes.
     */
    private final String TAG =
        this.getClass().getCanonicalName();

    /**
     * Parse the @a inputStream and convert it into a WeatherForecastListEntry of JsonWeather
     * objects.
     */
    public WeatherForecast parseForecastJsonStream(InputStream inputStream)
        throws IOException {
        try (JsonReader reader =
                     new JsonReader(new InputStreamReader(inputStream,
                             "UTF-8"))) {
             Log.d(TAG, "Parsing the results returned as an array");
             Log.d(TAG, "Reader consists of: " + reader.toString());

            // Handle the array returned from the Weather Service.
            return parseJsonForecastObject(reader);
        }
    }


    public WeatherForecast parseJsonForecastObject(JsonReader reader) throws IOException {

        WeatherForecast forecast = null;
        try {
            // If the data wasn't expanded return null;
            if (reader.peek() == JsonToken.END_OBJECT){
                Log.d(TAG, "Reader returned null for a forecast");
                return null;
            }

            if(reader.peek() == JsonToken.BEGIN_OBJECT){
                //while (reader.hasNext())
                forecast = parseJsonWeatherForecast(reader);
            }
            if (forecast == null){
                Log.d(TAG, "Returning null from weather parsing");
                return null;
            } else{
                Log.d(TAG, "Returning forecast: " + forecast.toString());

                return forecast;
            }

        } finally {
            reader.close();
        }
    }

    private WeatherForecast parseJsonWeatherForecast(JsonReader reader) throws IOException {

        WeatherForecast jsonWeatherForecastObject = new WeatherForecast();
        reader.beginObject();

        try {
            while (reader.hasNext()) {
                String name = reader.nextName();
                switch (name) {
                    case WeatherForecast.cod_JSON:
                        jsonWeatherForecastObject.setCod(reader.nextLong());
                        Log.d(TAG, "read cod field: " + jsonWeatherForecastObject.getCod());
                        break;
                    case WeatherForecast.city_JSON:
                        jsonWeatherForecastObject.setCity(parseCity(reader));
                        break;
                    case WeatherForecast.cnt_JSON:
                        jsonWeatherForecastObject.setCnt(reader.nextLong());
                        break;
                    case WeatherForecast.message_JSON:
                        jsonWeatherForecastObject.setMessage(reader.nextDouble());
                        break;
                    case WeatherForecast.list_JSON:
                        jsonWeatherForecastObject.setList(parseList(reader));
                        break;
                    default:
                        reader.skipValue();
                        Log.d(TAG, "A weird problem with " + name + " field");
                        break;
                }
            }
        } finally {
            reader.endObject();
        }
        return jsonWeatherForecastObject.getCod() == 404L?
                null : jsonWeatherForecastObject;
    }

    private City parseCity(JsonReader reader) throws IOException {
        City cityObject = new City();
        reader.beginObject();

        try {
            while (reader.hasNext()) {
                String name = reader.nextName();
                switch (name) {
                    case City.id_JSON:
                        cityObject.setId(reader.nextLong());
                        break;
                    case City.name_JSON:
                        cityObject.setName(reader.nextString());
                        break;
                    default:
                        reader.skipValue();
                        Log.d(TAG, "Skipped field with name " + name);
                        break;
                }
            }
        } finally {
            reader.endObject();
        }
        return  cityObject;
    }

    private List<WeatherForecastListEntry> parseList(JsonReader reader) throws IOException {
        List forecasts = new ArrayList();
        reader.beginArray();

        try {
            while (reader.hasNext()) {

                forecasts.add(parseWeatherForecastListEntry(reader));
            }
            return forecasts;
        } finally {
            reader.endArray();
        }
    }

    private WeatherForecastListEntry parseWeatherForecastListEntry(JsonReader reader)
            throws IOException {
        WeatherForecastListEntry entry = new WeatherForecastListEntry();
        reader.beginObject();
        try {
            while (reader.hasNext()){
                String name = reader.nextName();
                switch (name) {
                    case WeatherForecastListEntry.dt_JSON:
                        entry.setDt(reader.nextLong());
                        break;
                    case WeatherForecastListEntry.main_JSON:
                        entry.setMain(parseMain(reader));
                        break;
                    case WeatherForecastListEntry.weathers_JSON:
                        entry.setWeathers(parseWeathers(reader));
                        break;
                    case WeatherForecastListEntry.clouds_JSON:
                        entry.setClouds(parseClouds(reader));
                        break;
                    case WeatherForecastListEntry.wind_JSON:
                        entry.setWind(parseWind(reader));
                        break;
                    case WeatherForecastListEntry.sys_JSON:
                        entry.setSys(parseSys_(reader));
                        break;
                    case WeatherForecastListEntry.snow_JSON:
                        entry.setSnow(parseSnow(reader));
                        break;
                    case WeatherForecastListEntry.dtTxt_JSON:
                        entry.setDtTxt(reader.nextString());
                        break;
                    default:
                        reader.skipValue();
                        Log.d(TAG, "Skipped field with name " + name);
                        break;
                }
            }
        }
        finally {
            reader.endObject();
        }
        return entry;
    }

    private List<Weather> parseWeathers(JsonReader reader) throws IOException {
        reader.beginArray();

        try {
            List<Weather> weathers = new ArrayList<>();

            while (reader.hasNext())
                weathers.add(parseWeather(reader));

            return weathers;
        } finally {
            reader.endArray();
        }
    }

    private Weather parseWeather(JsonReader reader)
            throws IOException {
        reader.beginObject();

        Weather weatherObject = new Weather();
        try {
            while (reader.hasNext()){
                String name = reader.nextName();
                switch (name){
                    case Weather.id_JSON:
                        weatherObject.setId(reader.nextLong());
                        break;
                    case Weather.main_JSON:
                        weatherObject.setMain(reader.nextString());
                        break;
                    case Weather.description_JSON:
                        weatherObject.setDescription(reader.nextString());
                        break;
                    case Weather.icon_JSON:
                        weatherObject.setIcon(reader.nextString());
                        break;
                    default:
                        reader.skipValue();
                        Log.d(TAG, "Skipped field with name " + name);
                }
            }
        } finally {

            reader.endObject();
        }

        return weatherObject;
    }

    private Clouds parseClouds(JsonReader reader) throws IOException {
        reader.beginObject();
        Clouds clouds = new Clouds();

        try {
            while (reader.hasNext()) {
                String name = reader.nextName();
                if (Objects.equals(name, Clouds.all_JSON)) {
                    clouds.setAll(reader.nextLong());
                } else {
                    reader.skipValue();
                    Log.d(TAG, "Skipped field with name " + name);
                }
            }
        } finally {
            reader.endObject();
        }
        return clouds;
    }

    /**
     * Parse a Json stream and return a Main Object.
     */
    private Main parseMain(JsonReader reader)
        throws IOException {
        //"main":{"temp":306.15,"pressure":1013,"humidity":44,"temp_min":306,"temp_max":306}
        reader.beginObject();

        Main mainObject = new Main();
        try {
            while (reader.hasNext()){
                String name = reader.nextName();
                switch (name){
                    case Main.temp_JSON:
                        mainObject.setTemp(reader.nextDouble());
                        break;
                    case Main.humidity_JSON:
                        mainObject.setHumidity(reader.nextLong());
                        break;
                    case Main.tempMin_JSON:
                        mainObject.setTempMin(reader.nextDouble());
                        break;
                    case Main.tempMax_JSON:
                        mainObject.setTempMax(reader.nextDouble());
                        break;
                    case Main.pressure_JSON:
                        mainObject.setPressure(reader.nextDouble());
                        break;
                    case Main.seaLevel_JSON:
                        mainObject.setSeaLevel(reader.nextDouble());
                        break;
                    case Main.grndLevel_JSON:
                        mainObject.setGrndLevel(reader.nextDouble());
                        break;
                    case Main.tempKf_JSON:
                        mainObject.setGrndLevel(reader.nextDouble());
                        break;
                    default:
                        reader.skipValue();
                        Log.d(TAG, "Skipped field with name " + name);
                        break;
                }
            }
        } finally {
            reader.endObject();
        }
        return mainObject;
    }

    /**
     * Parse a Json stream and return a Wind Object.
     */
    private Wind parseWind(JsonReader reader) throws IOException {
        reader.beginObject();

        Wind windObject = new Wind();
        try {
            while (reader.hasNext()){
                String name = reader.nextName();
                switch (name){
                    case Wind.speed_JSON:
                        windObject.setSpeed(reader.nextDouble());
                        break;
                    case Wind.deg_JSON:
                        windObject.setDeg(reader.nextDouble());
                        break;
                    default:
                        reader.skipValue();
                        Log.d(TAG, "Skipped field with name " + name);
                        break;
                }
            }
        } finally {
            reader.endObject();
        }
        return windObject;
    }

    /**
     * Parse a Json stream and return a Sys Object.
     */
    private Sys_ parseSys_ (JsonReader reader) throws IOException {
        reader.beginObject();

        Sys_ sysObject = new Sys_();
        try{
            while (reader.hasNext()){
                String name = reader.nextName();
                switch (name){
                    case Sys_.pod_JSON:
                        sysObject.setPod(reader.nextString());
                        break;
                    default:
                        reader.skipValue();
                        Log.d(TAG, "Skipped field with name " + name);
                        break;
                }
            }
        } finally {
            reader.endObject();
        }
        return sysObject;
    }

    private Snow parseSnow(JsonReader reader) throws IOException {
        reader.beginObject();

        Snow snowObject = new Snow();
        try{
            while (reader.hasNext()){
                String name = reader.nextName();
                switch (name){
                    case Snow._3h_JSON:
                        snowObject.set3h(reader.nextDouble());
                        break;
                    default:
                        reader.skipValue();
                        Log.d(TAG, "Skipped field with name " + name);
                        break;
                }
            }
        } finally {
            reader.endObject();
        }
        return snowObject;
    }

}
