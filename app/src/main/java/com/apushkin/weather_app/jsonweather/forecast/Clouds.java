package com.apushkin.weather_app.jsonweather.forecast;

public class Clouds {

    public static final String all_JSON = "all";
    private Long all;

    /**
     * No args constructor for use in serialization
     *
     */
    public Clouds() {
    }

    /**
     *
     * @param all
     */
    public Clouds(Long all) {
        this.all = all;
    }

    /**
     *
     * @return
     * The all
     */
    public Long getAll() {
        return all;
    }

    /**
     *
     * @param all
     * The all
     */
    public void setAll(Long all) {
        this.all = all;
    }

}
