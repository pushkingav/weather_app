package com.apushkin.weather_app.jsonweather.forecast;

public class Wind {

    public final static String deg_JSON = "deg";
    public final static String speed_JSON = "speed";

    private Double speed;
    private Double deg;

    /**
     * No args constructor for use in serialization
     *
     */
    public Wind() {
    }

    /**
     *
     * @param speed
     * @param deg
     */
    public Wind(Double speed, Double deg) {
        this.speed = speed;
        this.deg = deg;
    }

    /**
     *
     * @return
     * The speed
     */
    public Double getSpeed() {
        return speed;
    }

    /**
     *
     * @param speed
     * The speed
     */
    public void setSpeed(Double speed) {
        this.speed = speed;
    }

    /**
     *
     * @return
     * The deg
     */
    public Double getDeg() {
        return deg;
    }

    /**
     *
     * @param deg
     * The deg
     */
    public void setDeg(Double deg) {
        this.deg = deg;
    }

}