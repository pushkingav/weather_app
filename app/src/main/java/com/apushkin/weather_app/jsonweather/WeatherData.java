package com.apushkin.weather_app.jsonweather;

import java.io.Serializable;

public class WeatherData implements Serializable {

    public String mDisplayName;
    public String mName;
    public double mSpeed;
    public double mDeg;
    public double mTemp;
    public long mHumidity;
    public long mSunrise;
    public long mSunset;
    public double mPressure;
    public String mRawJSON;


    public WeatherData(String displayName,
                       String name,
                       double speed,
                       double deg,
                       double temp,
                       long humidity,
                       long sunrise,
                       long sunset,
                       double pressure,
                       String rawJSON) {
        mDisplayName = displayName;
        mName = name;
        mSpeed = speed;
        mDeg = deg;
        mTemp = temp;
        mHumidity = humidity;
        mSunrise = sunrise;
        mSunset = sunset;
        mPressure = pressure;
        mRawJSON = rawJSON;
    }

    @Override
    public String toString() {
        return "WeatherData [display name = " + mDisplayName
                + "name= " + mName
                + ", speed= " + mSpeed
                + ", deg= " + mDeg
                + ", temp= " + mTemp
                + ", humidity= " + mHumidity
                + ", sunrise= " + mSunrise
                + ", sunset= " + mSunset + "]";
    }
}
