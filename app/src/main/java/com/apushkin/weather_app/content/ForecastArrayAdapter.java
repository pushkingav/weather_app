package com.apushkin.weather_app.content;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.apushkin.weather_app.R;

import static com.apushkin.weather_app.WeatherDetailsFragment.CityItem;


public class ForecastArrayAdapter extends ArrayAdapter<CityItem>{

        public ForecastArrayAdapter(Context context) {
            super(context, R.layout.forecast_row);
        }

        @Override
        public View getView(int position,
                            View convertView,
                            ViewGroup parent) {
            CityItem item = getItem(position);

            if (convertView == null)
                convertView =
                        LayoutInflater.from(getContext()).inflate
                                (R.layout.forecast_row, parent, false);

            TextView forecastDateTV =
                    (TextView) convertView.findViewById(R.id.date_forecast_text_view);
            forecastDateTV.setText(item.getDate());

            TextView forecastTempTV =
                    (TextView) convertView.findViewById(R.id.temp_forecast_text_view);
            forecastTempTV.setText(Math.round(item.getTemp()) + " \u2103");

            return convertView;
        }
}
