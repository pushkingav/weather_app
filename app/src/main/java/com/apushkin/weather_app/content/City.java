package com.apushkin.weather_app.content;

import com.apushkin.weather_app.jsonweather.WeatherData;

import java.io.Serializable;

public class City implements Serializable {
    //from JSON
    private String name;
    //user-facing name in Russian
    private String displayName;
    //for 2-word names with space between them
    private String uriName;

    private WeatherData weather;

    public City() {
    }

    public City(String name, WeatherData weather) {
        this.name = name;
        this.weather = weather;
    }

    public String getUriName() {
        return uriName;
    }

    public void setUriName(String uriName) {
        this.uriName = uriName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public WeatherData getWeather() {
        return weather;
    }

    public void setWeather(WeatherData weather) {
        this.weather = weather;
    }

}
