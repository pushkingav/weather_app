package com.apushkin.weather_app.content;


import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.apushkin.weather_app.R;

import java.util.ArrayList;
import java.util.List;

public class CitiesListArrayAdapter extends ArrayAdapter<City>{
        public CitiesListArrayAdapter(Context context) {
            super(context, R.layout.city_row);
        }

        @NonNull
        @Override
        public View getView(int position,
                            View convertView,
                            ViewGroup parent) {
            City item = getItem(position);

            if (convertView == null)
                convertView =
                        LayoutInflater.from(getContext()).inflate
                                (R.layout.city_row, parent, false);

            TextView cityDisplayNameTV =
                    (TextView) convertView.findViewById(R.id.city_name_tv);
            cityDisplayNameTV.setText(item != null ? item.getDisplayName() : "N/A");

            TextView cityTempTV =
                    (TextView) convertView.findViewById(R.id.temperature_tv);
            if (item != null) {

                cityTempTV.setText((item.getWeather() != null)
                        ? String.valueOf(Math.round(item.getWeather().mTemp)) + " \u2103"
                        : "N/A");
            }

            return convertView;
        }

    public City getItem(String displayName) {
        int count = this.getCount();
        List<City> adapterCities = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            adapterCities.add(getItem(i));
        }

        for (City city: adapterCities ) {
            if (displayName.equals(city.getDisplayName())){
                return city;
            }
        }
        return null;
    }

    public List<City> getCitiesFromAdapter(){
        int count = this.getCount();
        List<City> adapterCities = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            adapterCities.add(getItem(i));
        }
        return adapterCities;
    }

}
