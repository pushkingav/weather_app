package com.apushkin.weather_app.utils;
// inspired by Coursera lessons

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Point;
import android.os.IBinder;
import android.util.Log;
import android.view.Display;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.apushkin.weather_app.jsonweather.JsonWeather;
import com.apushkin.weather_app.jsonweather.WeatherData;
import com.apushkin.weather_app.jsonweather.WeatherJSONParser;
import com.apushkin.weather_app.jsonweather.forecast.ForecastJSONParser;
import com.apushkin.weather_app.jsonweather.forecast.WeatherForecast;
import com.apushkin.weather_app.storage.Cache;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public class Utils {

    private final static String TAG = Utils.class.getCanonicalName();

    /**
     * URL to the Open Weather Map web service.
     */
    private static final String sWeather_Web_Service_URL =
        "http://api.openweathermap.org/data/2.5/weather?units=metric&q=";

    private static final String sForecast_Web_Service_URL =
            "http://api.openweathermap.org/data/2.5/forecast?units=metric&q=";

    private final static String KEY = "7fdbd1f3d3fdd49668c1243183698d6f";

    public static List<WeatherData> getResults(final String cityName) {

        final List<WeatherData> returnList =
            new ArrayList<WeatherData>();
            
        // A WeatherForecastListEntry of JsonWeather objects.
        List<JsonWeather> jsonWeathers = null;

        String nameWithConvertedSpace = convertSpaces(cityName);
        String JSON_str = "";

        try {
            // Append the location to create the full URL.
            final URL url =
                new URL(sWeather_Web_Service_URL
                        + nameWithConvertedSpace + "&APPID=" + KEY);

            // Opens a connection to the Acronym Service.
            HttpURLConnection urlConnection =
                (HttpURLConnection) url.openConnection();
            // Sends the GET request and reads the Json results.
            try (InputStream in =
                 new BufferedInputStream(urlConnection.getInputStream())) {

                JSON_str = saveStream(in);
                Log.d(TAG, "Raw JSON: " + JSON_str);

            } finally {
                urlConnection.disconnect();
            }
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        InputStream in = new ByteArrayInputStream(JSON_str.getBytes(StandardCharsets.UTF_8));
        // Create the parser.
        final WeatherJSONParser parser =
                new WeatherJSONParser();

        // Parse the Json results and create JsonWeathers data
        // objects.
        try {
            jsonWeathers = parser.parseJsonStream(in);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (jsonWeathers != null)
            Log.d(TAG, "Parsed weather: " + jsonWeathers.toString());

            if (jsonWeathers != null)
                Log.d(TAG, "jsonWeathers.size = " + jsonWeathers.size());

        if (jsonWeathers != null && jsonWeathers.size() > 0) {
            // Convert the JsonWeather data objects to our WeatherData
            // object, which can be passed between processes.
            for (JsonWeather jsonWeather : jsonWeathers)
                returnList.add(new WeatherData(cityName,
                                               jsonWeather.getName(),
                                               jsonWeather.getWind().getSpeed(),
                                               jsonWeather.getWind().getDeg(),
                                               jsonWeather.getMain().getTemp(),
                                               jsonWeather.getMain().getHumidity(),
                                               jsonWeather.getSys().getSunrise(),
                                               jsonWeather.getSys().getSunset(),
                                               jsonWeather.getMain().getPressure(),
                                               JSON_str));

             return returnList;
        }  else
            return null;
    }

    public static String convertSpaces(String oldName){
        return oldName.replace(" ", "%20");
    }

    private static String saveStream(InputStream in) throws IOException{
        byte[] contents = new byte[1024];

        int bytesRead = 0;
        String strFileContents = "";
        while((bytesRead = in.read(contents)) != -1) {
            strFileContents += new String(contents, 0, bytesRead);
        }
        return strFileContents;
    }

    public static List<WeatherData> parseResultsFromSavedJSON(String json, String cityName){
        if (Objects.equals(json, "")){
            Log.d(TAG, "Started to parse null as a json");
            return null;
        }

        InputStream in = new ByteArrayInputStream(json.getBytes(StandardCharsets.UTF_8));

        // Create the parser.
        final WeatherJSONParser parser = new WeatherJSONParser();
        final List<WeatherData> returnList = new ArrayList<>();
        List<JsonWeather> jsonWeathers = null;

        try {
            jsonWeathers = parser.parseJsonStream(in);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (jsonWeathers != null)
            Log.d(TAG, "Parsed saved weather: " + jsonWeathers.toString());

        if (jsonWeathers != null)
            Log.d(TAG, "jsonWeathers.size = " + jsonWeathers.size());

        if (jsonWeathers != null && jsonWeathers.size() > 0) {
            // Convert the JsonWeather data objects to our WeatherData
            // object, which can be passed between processes.
            for (JsonWeather jsonWeather : jsonWeathers)
                returnList.add(new WeatherData(cityName,
                        jsonWeather.getName(),
                        jsonWeather.getWind().getSpeed(),
                        jsonWeather.getWind().getDeg(),
                        jsonWeather.getMain().getTemp(),
                        jsonWeather.getMain().getHumidity(),
                        jsonWeather.getSys().getSunrise(),
                        jsonWeather.getSys().getSunset(),
                        jsonWeather.getMain().getPressure(),
                        ""));

            return returnList;
        }  else
            return null;
    }


    public static WeatherForecast getForecast(final String cityName) {

        WeatherForecast forecast = null;
        String nameWithConvertedSpace = convertSpaces(cityName);
        String forecast_JSON = "";


        try {
            // Append the location to create the full URL.
            final URL url =
                    new URL(sForecast_Web_Service_URL
                            + nameWithConvertedSpace + "&APPID=" + KEY);

            HttpURLConnection urlConnection =
                    (HttpURLConnection) url.openConnection();

            // Sends the GET request and reads the Json results.
            try (InputStream in =
                         new BufferedInputStream(urlConnection.getInputStream())) {

                forecast_JSON = saveStream(in);
                if (forecast_JSON !=null && !forecast_JSON.isEmpty()){
                    Log.d(TAG, "Saved forecast as a JSON String: " + forecast_JSON);
                }
            } finally {
                urlConnection.disconnect();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        InputStream in = new ByteArrayInputStream(forecast_JSON.getBytes(StandardCharsets.UTF_8));
        final ForecastJSONParser parser = new ForecastJSONParser();

        try {
            forecast = parser.parseForecastJsonStream(in);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (forecast != null){
            Log.d(TAG, "Forecast was downloaded and parsed");
            forecast.setRaw_Json(forecast_JSON);
            forecast.setDisplayName(cityName);
        }

        return forecast;
    }

    public static WeatherForecast parseForecastFromCache(final String json) {
        if (Objects.equals(json, "")){
            Log.d(TAG, "Started to parse null as a json for forecast");
            return null;
        }

        // Create the parser:
        final ForecastJSONParser parser = new ForecastJSONParser();
        WeatherForecast forecast = new WeatherForecast();

        //Convert String to InputStream and parse it:
        InputStream in = new ByteArrayInputStream(json.getBytes(StandardCharsets.UTF_8));

        try {
            forecast = parser.parseForecastJsonStream(in);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (forecast != null){
                Log.d(TAG, "Forecast was parsed from cache");
                forecast.setRaw_Json(json);
            }
        return forecast;
    }

    /**
     * This method is used to hide a keyboard after a user has
     * finished typing the url.
     */
    public static void hideKeyboard(Activity activity,
                                    IBinder windowToken) {
        InputMethodManager mgr =
           (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        mgr.hideSoftInputFromWindow(windowToken, 0);
    }

    public static int getScreenOrientation(Activity activity){
        Display getOrient = activity.getWindowManager().getDefaultDisplay();
        int orientation;
        Point display = new Point();
        getOrient.getSize(display);
        if(display.x  < display.y){
            orientation = Configuration.ORIENTATION_PORTRAIT;
        }else {
            orientation = Configuration.ORIENTATION_LANDSCAPE;
        }
        return orientation;
    }

    /**
     * Show a toast message.
     */
    public static void showToast(Context context,
                                 String message) {
        Toast.makeText(context,
                       message,
                       Toast.LENGTH_SHORT).show();
    }

    public static void saveWeatherForecastToCache(WeatherForecast forecast, String theKey){
        Cache.getInstance().getLru().put(theKey, forecast);
    }

    public static WeatherForecast retrieveWeatherForecastFromCache(String theKey){
        return (WeatherForecast) Cache.getInstance().getLru().get(theKey);
    }

    public static void saveAndReplaceSmthInCache(Object object, String theKey){
        Cache.getInstance().getLru().remove(theKey);
        Cache.getInstance().getLru().put(theKey, object);
    }

    public Object getSmthFromCache(String theKey){
        return Cache.getInstance().getLru().get(theKey);
    }


    /**
     * Ensure this class is only used as a utility.
     */
    private Utils() {
        throw new AssertionError();
    }
}
