package com.apushkin.weather_app.utils;
// http://www.androiddesignpatterns.com/2013/04/retaining-objects-across-config-changes.html

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.apushkin.weather_app.MainActivity;
import com.apushkin.weather_app.R;
import com.apushkin.weather_app.jsonweather.WeatherData;
import com.apushkin.weather_app.jsonweather.forecast.WeatherForecast;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import static com.apushkin.weather_app.MainActivity.JSONType.FORECAST;
import static com.apushkin.weather_app.MainActivity.JSONType.WEATHER;

public class TaskFragment extends Fragment {
    public static final String TAG = "Task Fragment";
    private static final String CITIES_KEY  = "CitiesTaskFragBundle";
    private WeakReference<MainActivity> mActivity;
    private boolean mForce = false;

    private ArrayList<String> mCities;

    public interface WeatherTaskCallbacks {
        void onCancelled();
        void onPostExecute(List<WeatherData> results, String errorMessage);
    }

    public interface ForecastTaskCallbacks {
        void onPostExecute(WeatherForecast result, String errorMessage);
    }

    private WeatherTaskCallbacks weatherTaskCallbacks;
    private ForecastTaskCallbacks forecastTaskCallbacks;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        weatherTaskCallbacks = (WeatherTaskCallbacks) activity;
        forecastTaskCallbacks = (ForecastTaskCallbacks) activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = new WeakReference<>((MainActivity) getActivity());

        mCities = getArguments().getStringArrayList(CITIES_KEY);
        // Retain this fragment across configuration changes.
        setRetainInstance(true);

        for (String cityName: mCities) {
            WeatherTask task = new WeatherTask();
            task.execute(cityName);
        }
    }

    @SuppressWarnings("unused")
    public static TaskFragment newInstance(ArrayList<String> cities) {
        TaskFragment fragment = new TaskFragment();
        Bundle args = new Bundle();
        args.putStringArrayList(CITIES_KEY, cities);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        weatherTaskCallbacks = null;
    }

    public void runWeatherTaskForNewCity(String cityName) {
        WeatherTask task = new WeatherTask();
        task.execute(cityName);
    }

    public void runForecastTask(String cityName, boolean forceDownload){
        mForce = forceDownload;
        ForecastTask forecastTask = new ForecastTask();
        forecastTask.execute(cityName);
    }

    private class WeatherTask extends AsyncTask<String, Void, List<WeatherData>> {
        @Override
        protected List<WeatherData> doInBackground(String... params) {
            final String cityName = params[0];
            Log.d(TAG, "Starting weather query for " + cityName);
            String savedWeather = "";

            //if an activity is available and there is no need to force fetching results
            //via HTTP call...
            if (mActivity != null && !mForce) {
                savedWeather = mActivity.get()
                        .getSavedItemForCity(cityName, WEATHER);
            }

            //if there is a saved weather JSON string - use it
            if(savedWeather!=null && !savedWeather.isEmpty() && !mForce){
               mForce = false;
               return Utils.parseResultsFromSavedJSON(savedWeather, cityName);
            } else {
               mForce = false;
               return Utils.getResults(cityName);
            }
        }

        @Override
        protected void onCancelled() {
            if (weatherTaskCallbacks != null) {
                weatherTaskCallbacks.onCancelled();
            }
        }

        @Override
        protected void onPostExecute(List<WeatherData> results) {
            super.onPostExecute(results);
            if (weatherTaskCallbacks != null) {
                weatherTaskCallbacks.onPostExecute(results, getString(R.string.no_results));
            }
        }
    }

    private class ForecastTask extends AsyncTask<String, Void, WeatherForecast> {
        @Override
        protected WeatherForecast doInBackground(String... params) {
            final String cityName = params[0];
            Log.d(TAG, "Starting forecast query for " + cityName);
            String savedForecast = "";
            if (mActivity != null && !mForce) {
                savedForecast = mActivity.get()
                        .getSavedItemForCity(cityName, FORECAST);
            }
            if(savedForecast!=null && !savedForecast.isEmpty() && !mForce) {
                mForce = false;
                return Utils.parseForecastFromCache(savedForecast);
            } else {
                mForce = false;
                return Utils.getForecast(cityName);
            }
        }

        @Override
        protected void onPostExecute(WeatherForecast weatherForecast) {
            super.onPostExecute(weatherForecast);
            if (forecastTaskCallbacks != null){
                forecastTaskCallbacks.onPostExecute(weatherForecast,
                        getString(R.string.no_results));
            }
        }
    }
}
